<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class ArticleController extends Controller
{
    public function index()
    {
        return Article::all();
    }

    /*
    public function show($id)
    {
        return Article::find($id);
    }*/
    public function show(Article $article)
    {
        return $article;
    }

    public function store(Request $request)
    {
        $article = Article::create($request->all());
        return response()->json($article, 201);
    }

    public function update(Request $request,/*$id*/ Article $article){
        //$article = Article::findOrFail($id);
        $article::update($request->all());
        //return $article;
        return response()->json($article, 200);
    }

    public function delete(Article $article/*$id*/){
        //$article = Article::find($id);
        $article->delete();
        //return 204; 
        return response()->json(null, 204);
    }
}
